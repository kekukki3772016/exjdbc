package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static dao.Dao.URL;

/**
 * Created by root on 2/11/17.
 */
public class Person {

    public static List<jdbc.Person> getPersons() throws SQLException {

        List<jdbc.Person> personList = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(URL);
             Statement stmt = conn.createStatement()) {

            try(ResultSet resultSet = stmt.executeQuery("SELECT * FROM person")){

                while (resultSet.next()){
                    jdbc.Person person = new jdbc.Person(
                            resultSet.getLong(1),
                            resultSet.getString(2),
                            resultSet.getInt(3) );
                    personList.add(person);
                }
            }

            return personList;
        }
    }

    public static Person getPersonById(String id) throws SQLException {
        try (Connection conn = DriverManager.getConnection(URL);
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM person WHERE id = ?")){
            ps.setString(1, id);
            

        }
    }

    public static void createTwoPersons() throws SQLException {
        try (Connection conn = DriverManager.getConnection(URL);
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate("insert into person (id, name) values (1, 'Kert')");
            stmt.executeUpdate("insert into person (id, name) values (2, 'Martina')");
        }
    }

    public static void createTable() throws SQLException {
        try (Connection conn = DriverManager.getConnection(URL);
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate("CREATE TABLE PERSON (\n" +
                    "      id BIGINT NOT NULL PRIMARY KEY,\n" +
                    "      name VARCHAR(255) NOT NULL,\n" +
                    "      age int\n" +
                    "   );");
        }
    }

}
